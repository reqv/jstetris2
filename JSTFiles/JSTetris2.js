/*
===============================================================
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$=
============================================================  =
	JSTetris2 created by Wojciech Janeczek (reqv)
		Feel free to copy or modyfy this file but
			please leave this description untouched.
				( year of production: 08/2013(oryginal JST game), version: 2.0 (2017) )

						Thank you for using my application.
						Visit github.com/reqv for more app`s and leave me a star if you like them :)
============================================================  =
$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$=
===============================================================
*/
(function(){
	// Variables
	let surface = null;													// Game table surface
	let context = null;													//  ^ 2d view of it
	let panel = null;													// Game interface DIV
	let infoField = null;												// Displayed if player score tetris
	
	let mainBTN = null;													// Main button to start/stop game
	let scoreField = null;
	let score = 0;
	let thousandAt = 1000;												// At this score you will get "thousand" bonus
	let isCleanBonusActivated = false;									// Clean bonus flag
	let nextSurface = null;												// Surface for next block preview
	let nextSurfaceContext = null;										// ^ 2d view of it
	let bestScoreField = null;
	let musicPlayer = null; 											// Hidden music player
	let musicBTN = null;												// Button for music handle
	let soundScorePlayer = null;										// Hidden 'score' effect player
	let	soundTetrisPlayer = null;										// Hidden 'tetris' effect player
	let	soundBTN = null;												// Button for sound effects handle

	let controller = false;												// User input checks
	let rotationAllowed = true;											// Flag that permit rotation of the block
	
	let timer;															// Time loop clock and blocks acceleration clock
	let accelerationTimer;
	let time = 0;
	
	let paused = false;													// Check if game is paused...
	let gameEnds = false;												// ended...
	let accelerateAfterPause = false;									// or there is loop acceleration needed
	
	let actualFigure = 0;												// Figures id`s
	let smallFigure = 0;
	let nextFigure = 0;
	
	let table = [];														// Array for table
	let block = [
		{ x: 0, y: 0, x_off: 0, y_off: 0, color: 0 },
		{ x: 0, y: 0, x_off: 0, y_off: 0, color: 0 },
		{ x: 0, y: 0, x_off: 0, y_off: 0, color: 0 },
		{ x: 0, y: 0, x_off: 0, y_off: 0, color: 0 }
	];
	
	let smallBlockImg = new Image();
	let gameEndsImg = new Image();
	
	let brick = {x:0,y:0,x_off:0,y_off:0,color:0};
	
	// Constants
	const howManyFigures = 28;											// How many there is ?
	const horizontalTableBlocks = 12;									// Max blocks in horizontal
	const verticalTableBlocks = 16;										// Max blocks in vertical
	
	const loopAccelerationTime = 120000;								// After this period of time (in ms), blocks starts to fall faster
	
	const mainButtonPlayText = 'Play!';									// Strings
	const mainButtonStopText = 'Stop!';
	const soundBTNText = 'Effects';
	const musicBTNText = 'Music';
	const onString = ' <b>On</b>/Off';
	const offString = ' On/<b>Off</b>';
	
	const mainFolder = 'JSTFiles/';										// This is where all game files are
	
	const blockType = [													// Block Types
		{ name: 'yellow', imageObject: new Image() },
		{ name: 'blue', imageObject: new Image() }
	];
	
	// Create and Configure
	 init();

	// #################
	// ### FUNCTIONS ###
	
	function init(){	// Initialize and configure
		let _mainWindow = document.getElementById("JST-main");
		
		// Panel
		panel = document.createElement("div");
		panel.id = "JST-panel";
		_mainWindow.appendChild(panel);
		
		// Game Surface
		surface = document.createElement("canvas");
		surface.id = "JST-surface";
		surface.width = 480;
		surface.height = 640;
		surface.innerHTML = "<b>Your browser can`t handle HTML5 and Canvas elements. Please update your browser!</b>";
		context = surface.getContext("2d");
		_mainWindow.appendChild(surface);
		
		// Info DIV
		infoField = document.createElement("div");
		infoField.id = "JST-info";
		infoField.className = "hidden";
		_mainWindow.appendChild(infoField);
	
		// Logo Image
		let _logo = document.createElement("img");
		_logo.id = "JST-logo";
		_logo.src = mainFolder+"logo.png";
		_logo.alt = "JST Logo";
		panel.appendChild(_logo);
		
		// Play Button
		mainBTN = document.createElement("a");
		mainBTN.href = "javascript:void(0)";
		mainBTN.id = "JST-play";
		mainBTN.innerHTML = mainButtonPlayText;
		mainBTN.onclick = Start;
		panel.appendChild(mainBTN);
		
		// Music Button
		musicBTN = document.createElement("a");
		musicBTN.href = "javascript:void(0)";
		musicBTN.id = "JST-music-button";
		musicBTN.classList.add('disabled');
		if (localStorage.getItem("music") == undefined || localStorage.getItem("music") === 'off') {
			localStorage.setItem("music", "off");
			musicBTN.innerHTML = musicBTNText + offString;
		} else {
			musicBTN.innerHTML = musicBTNText + onString;
		}
		musicBTN.onclick = Music;
		panel.appendChild(musicBTN);
		
		// Music Player
		musicPlayer = document.createElement("audio");
		musicPlayer.setAttribute("loop", "true");
		musicPlayer.setAttribute("preload", "true");
		let _musicPlayerSource = document.createElement("source");
		_musicPlayerSource.src = mainFolder + "music.ogg";
		_musicPlayerSource.type = "audio/ogg";
		_musicPlayerSource.innerHTML = "Your browser can`t handle ogg music...";
		musicPlayer.appendChild(_musicPlayerSource);
		panel.appendChild(musicPlayer);

		// Sound Button
		soundBTN = document.createElement("a");
		soundBTN.href = "javascript:void(0)";
		soundBTN.id = "JST-sound-button";
		soundBTN.classList.add('disabled');
		if (localStorage.getItem("sounds") == undefined || localStorage.getItem("sounds") === "on") {
			localStorage.setItem("sounds", "on");
			soundBTN.innerHTML = soundBTNText + onString;
		} else {
			soundBTN.innerHTML = soundBTNText + offString;
		}
		soundBTN.onclick = SoundEffects;
		panel.appendChild(soundBTN);
		
		// Effect Player - point
		soundScorePlayer = document.createElement("audio");
		soundScorePlayer.setAttribute("preload", "true");
		let _soundScorePlayerSource = document.createElement("source");
		_soundScorePlayerSource.src = mainFolder + "point.ogg";
		_soundScorePlayerSource.type = "audio/ogg";
		_soundScorePlayerSource.innerHTML = "Your browser can`t handle ogg files...";
		soundScorePlayer.appendChild(_soundScorePlayerSource);
		panel.appendChild(soundScorePlayer);
		
		// Effect Player - tetris
		soundTetrisPlayer = document.createElement("audio");
		soundTetrisPlayer.setAttribute("preload", "true");
		let _soundTetrisPlayerSource = document.createElement("source");
		_soundTetrisPlayerSource.src = mainFolder + "tetris.ogg";
		_soundTetrisPlayerSource.type = "audio/ogg";
		_soundTetrisPlayerSource.innerHTML = "Your browser can`t handle ogg files...";
		soundTetrisPlayer.appendChild(_soundTetrisPlayerSource);
		panel.appendChild(soundTetrisPlayer);
		
		// 'hr' Line
		panel.appendChild(document.createElement("hr"));
		
		// Score Strings
		let _stringTemp = document.createElement("p");
		_stringTemp.className = "strings";
		_stringTemp.innerHTML = "SCORE:";
		panel.appendChild(_stringTemp);
		
		scoreField = document.createElement("p");
		scoreField.id = "JST-score";
		panel.appendChild(scoreField);
		
		// Best Score Strings
		_stringTemp = document.createElement("p");
		_stringTemp.className = "strings";
		_stringTemp.innerHTML = "Personal best score:";
		panel.appendChild(_stringTemp);
		
		bestScoreField = document.createElement("p");
		bestScoreField.id = "JST-best";
		scoreField.innerHTML = score;
		if(localStorage.getItem('bestScore') == undefined)
			localStorage.setItem("bestScore", 0);
		else
			bestScoreField.innerHTML = localStorage.getItem("bestScore");
		panel.appendChild(bestScoreField);
		
		// Next Block Surface and String
		_stringTemp = document.createElement("p");
		_stringTemp.className = "strings";
		_stringTemp.innerHTML = "NEXT BLOCK";
		panel.appendChild(_stringTemp);
		
		nextSurface = document.createElement("canvas");
		nextSurfaceContext = nextSurface.getContext("2d");
		nextSurface.id = "JST-next";
		nextSurface.width = 80;
		nextSurface.height = 80;
		nextSurface.innerHTML = "Can`t display the next block.";
		panel.appendChild(nextSurface);

		// Prepare blocks image files
		for(let type of blockType) {
			type.imageObject.src = `${mainFolder}blocks/${type.name}.png`;
		}
		
		smallBlockImg.src = mainFolder + 'smallblock.png';
		gameEndsImg.src = mainFolder + 'gameover.jpg';
	}
	
	// Starts the game
	function Start(){
		musicBTN.classList.remove('disabled');
		soundBTN.classList.remove('disabled');
		if (localStorage.getItem("music") === "on") {
			musicPlayer.play();
		}
		score = 0;
		accelerateAfterPause = false;
		time = 1000;
		scoreField.innerHTML = score;
		mainBTN.innerHTML = mainButtonStopText;
		mainBTN.onclick = Stop;
		actualFigure = Math.floor((Math.random() * howManyFigures) + 1);
		smallFigure = actualFigure;
		nextFigure = Math.floor((Math.random() * howManyFigures) + 1);
		
		// Initiate table with no blocks (-1)
		for (let i = 0; i < verticalTableBlocks; i++) {
			table[i] = new Array(horizontalTableBlocks).fill(-1);	//vertical table blocks
		}
		
		GetNewFigure(actualFigure, 5, 0, false);
		ShowNextFigure();
		controller = false;
		Draw();
		
		// Input events handlers
		document.addEventListener('keydown', PlayerInput, false);
		document.addEventListener('keyup', PlayerInput, false);
		
		// Main Loop
		timer = setInterval(MainLoop, time);
		accelerationTimer = setInterval(AccelerateLoop, loopAccelerationTime);
	}
	
	function Stop(){	// Stop the game
		if (paused) {
			return;
		}
		musicPlayer.load();
		musicBTN.classList.add('disabled');
		soundBTN.classList.add('disabled');
		mainBTN.innerHTML = mainButtonPlayText;
		mainBTN.onclick = Start;
		document.removeEventListener('keydown', PlayerInput, false);
		document.removeEventListener('keyup', PlayerInput, false);
		
		context.clear();
		nextSurfaceContext.clear();
		clearInterval(timer);
		clearInterval(accelerationTimer);
		
		if (score > localStorage.getItem("bestScore")) {
			localStorage.setItem("bestScore", score);
		}
		bestScoreField.innerHTML = localStorage.getItem("bestScore");
	}
	
	function MainLoop(){	// Main loop of the application
		if (!paused) {
			CheckBlocks();
			Move('down');
			if (gameEnds) {
				Gameover();
			}
		}
	}
	
	function Gameover(){	// This is when we lose
		gameEnds = false;
		Stop();
		for (let q1 = 0; q1 < horizontalTableBlocks; q1++) {
			for (let q2 = 0; q2 < verticalTableBlocks; q2++) {
				context.drawImage(blockType[0].imageObject, q1 * 40, q2 * 40);
			}
		}
		context.drawImage(gameEndsImg, (12 * 40) / 2 - 128, (16 * 40) / 2 - 76);
	}
	
	function AccelerateLoop(){
		if (paused) {
			accelerateAfterPause = true;
			return;
		}
		if (time > 100) {
			time -= 100;
			if (accelerateAfterPause && time > 100) {
				time -= 100;
				accelerateAfterPause = false;
			}
		}
		clearInterval(timer);
		timer = setInterval(MainLoop, time);
	}
	
	function CheckBlocks(){	//16 - vertical , 12 - horizontal
		let _counter = 0;
		let _scoreLines = [];
		let _lineClean = false;
		let _clean = true;
		for (let i = verticalTableBlocks - 1; i >= 0; i--) {
			for (let j = horizontalTableBlocks - 1; j >= 0; j--) {
				/*if(j === 0 && table[i][j] == 1){		//TODO: make this work :(
					gameEnds = true;
					return;
				}*/
				if (table[j][i] !== -1) {
					if (i < verticalTableBlocks / 2) {
						isCleanBonusActivated = true;
					}
					_lineClean = false;
					_counter++;
				}
			}
			if (_counter === horizontalTableBlocks) {
				_lineClean = true;
				_scoreLines.push(i);
			}
			if (!_lineClean) {
				_clean = false;
			}
			_counter = 0;
		}
		if (_scoreLines.length > 0) {
			Scored(_scoreLines, _clean);
		}
	}
	
	function Scored(lines = [], clean = false){
		let _offset = 0;
		let _effects = 0;
		lines.forEach(function(item){
			score += 10;
			for (let i = 0; i < horizontalTableBlocks; i++) {
				table[i][item + _offset] = -1;
			}
			for (let j = item + _offset; j > 0; j--) {
				for (let k=0; k < verticalTableBlocks; k++) {
					table[k][j] = table[k][j-1];
				}
			}
			_offset++;
		});
		if (lines.length > 3) {
			score+=40;
			_effects = 1;
		}
		if (clean && isCleanBonusActivated) {
			score += 150;
			_effects = 3;
			isCleanBonusActivated = false;
		}
		if (score >= thousandAt) {
			thousandAt += 1000;
			score += 20 * lines.length;
			for (let i = 0; i < horizontalTableBlocks; i++) {
				for (let j = 0; j < verticalTableBlocks; j++) {
					table[i][j] = -1;
					if (j === verticalTableBlocks - 1 && (i === 0 || i === horizontalTableBlocks - 1)) {
						table[i][j] = 0;
					}
					if (j === verticalTableBlocks - 2 && (i !== 0 && i !== horizontalTableBlocks - 1)) {
						table[i][j] = 1;
					}
				}
			}
			_effects = 2;
		}
		if (_effects > 0) {
			MakeToast(_effects);
		}
		PlaySoundEffect(_effects);
		scoreField.innerHTML = score;
	}
	
	function MakeToast(which = 0){
		switch (which) {
			case 1:
				infoField.style.backgroundImage = "url(" + mainFolder + "scoreTetris.png)";
				break;
			case 2:
				infoField.style.backgroundImage = "url(" + mainFolder + "scoreThousand.png)";
				break;
			case 3:
				infoField.style.backgroundImage = "url(" + mainFolder + "scoreCleaned.png)";
			default: break;
		}
		infoField.classList.remove('hidden');
		setTimeout(function(){
			infoField.classList.add('hidden');
		}, 3000);
	}
	
	function PlaySoundEffect(effectNumber){
		if (localStorage.getItem("sounds") === "off") {
			return;
		}
		switch (effectNumber) {
			case 0: 
				soundScorePlayer.play();
				break;
			case 1: 
				soundTetrisPlayer.play();
				break;
			default: break;
		}
	}
	
	function Draw(){	// Draw all blocks to canvas
		context.clear();
		for  (let actualBlock of block) {
			context.drawImage(blockType[actualBlock.color].imageObject, 0 + actualBlock.x * 40, 0 + actualBlock.y * 40);
		}
		for (let i = 0; i < horizontalTableBlocks; i++) {
			for (let j = 0; j < verticalTableBlocks; j++) {
				if (table[i][j] !== -1) {
					context.drawImage(blockType[table[i][j]].imageObject, i * 40, j * 40);
				}
			}
		}
	}
	
	function PlayerInput(e){	// Player input event controller
		if (e.type === 'keydown') {
			switch (e.keyCode) {
				// Left Key
				case 37:
					Move('left');
					break;
					
				// Up Key
				case 38:
					if (rotationAllowed) {
						GetNewFigure(actualFigure, block[0].x, block[0].y); 
						Draw();
						rotationAllowed = false;
					}
					break;
					
				// Right Key
				case 39:
					Move('right');
					break;
					
				// Down Key
				case 40:
					if (!controller) {
						Move('down');
					}
					break;
					
				// Pause Key
				case 80:
					PauseGame();
					break;
					
				// TODO: remove this before relase (for testing purposes)
				case 84: 
					console.log("TEST 84 PlayerInput!");
					score=990; scoreField.innerHTML = score;
					thousandAt= 1000;
					break;
			}
		}
		if (e.type === 'keyup') {
			switch (e.keyCode) {
				
				// Down Key
				case 38: 
					rotationAllowed = true;
					break;
				case 40:
					controller = false;
					break;
			}
		}
		e.preventDefault();
	}
	
	function Move(where){
		if (paused) {
			PauseGame();
		}
		let _ok = true;
		if (!paused) {
			switch (where) {
				case 'left': 
					for (let blk of block) {
						if (blk.x - 1 < 0 || table[blk.x - 1][blk.y] !== -1) {
							_ok = false;
						}
					}
					if (_ok === true) {
						for (let blk of block) {
							blk.x -= 1;
						}
					}
					break;
				case 'right':
					for (let blk of block) {
						if (blk.x + 1 >= horizontalTableBlocks || table[blk.x + 1][blk.y] !== -1) {
							_ok = false;
						}
					}
					if (_ok === true) {
						for (let blk of block) {
							blk.x += 1;
						}
					}
					break;
				case 'down':
					for (let blk of block) {
						// checks if on the ground
						if (blk.y >= verticalTableBlocks - 1 || table[blk.x][blk.y + 1] !== -1) {
							for (let blkToGround of block) {
								table[blkToGround.x][blkToGround.y] = blkToGround.color;
							}
							smallFigure = nextFigure;
							GetNewFigure(nextFigure, 5, 0, true, true);
							actualFigure = nextFigure;
							nextFigure = Math.floor((Math.random() * howManyFigures) + 1);
							ShowNextFigure();
							Draw();
							return;
						}
					}
					for (let blk of block) {
						blk.y += 1;
					}
					break;
			}
		}
		Draw();
	}
	
	function PauseGame(){
		if (paused) {
			paused = false;
			surface.style.background = "rgba(0,0,0,0.2)"
		} else {
			surface.style.background = "rgba(0,0,0,0.8)"
			context.clear();
			paused = true;
		}
	}
	
	function ShowNextFigure(){
		let _x = 1;
		let	_y = 0;
		
		nextSurfaceContext.clear();
		// TODO: delete this in the future ( workaround for figure 7)
		switch (nextFigure) {
			case 7: _y++; break;
		}
		GetNewFigure(nextFigure, _x, _y, false);
		for(let blk of block)
		{
			nextSurfaceContext.drawImage(smallBlockImg, 20 * blk.x, 20 * blk.y);
		}
		GetNewFigure(smallFigure, 5, 0, false);
	}
	
	function GetNewFigure(figure, x, y, validation = true, isNewFigure = false){
		if (paused) {
			PauseGame();
		}
		let _ok = true;
		let _hexFigure = '2000';
		
		switch (figure) {
			case 1:
			case 24:
			case 25:
			case 26:
				_hexFigure = '3300';
				actualFigure = 1;
				break;
			case 2:
			case 27:
				_hexFigure = 'F000';
				actualFigure = 3;
				break;
			case 3:
			case 28:
				_hexFigure = '2222';
				actualFigure = 2;
				break;
			case 4:
				_hexFigure = '2260';
				actualFigure = 5;
				break;
			case 5:
				_hexFigure = '7100';
				actualFigure = 6;
				break;
			case 6:
				_hexFigure = '6440';
				actualFigure = 7;
				break;
			case 7:
				_hexFigure = '4700';
				actualFigure = 4;
				break;
			case 8:
				_hexFigure = '2230';
				actualFigure = 9;
				break;
			case 9:
				_hexFigure = '2E00';
				actualFigure = 10;
				break;
			case 10:
				_hexFigure = '3110';
				actualFigure = 11;
				break;
			case 11:
				_hexFigure = '7400';
				actualFigure = 8;
				break;
			case 12:
				_hexFigure = '2700';
				actualFigure = 13;
				break;
			case 13:
				_hexFigure = '2620';
				actualFigure = 14;
				break;
			case 14:
				_hexFigure = '7200';
				actualFigure = 15;
				break;
			case 15:
				_hexFigure = '2320';
				actualFigure = 12;
				break;
			case 16:
			case 17:
				_hexFigure = '3600';
				actualFigure = 18;
				break;
			case 18:
			case 19:
				_hexFigure = '2310';
				actualFigure = 16;
				break;
			case 20:
			case 21:
				_hexFigure = '6300';
				actualFigure= 22;
				break;
			case 22:
			case 23:
				_hexFigure = '2640';
				actualFigure= 20;
				break;
			
		}
		HexToFigure(_hexFigure, x, y, isNewFigure);
		if (!validation) {
			controller = true;		// check if user can use controlls
		} else {
			for (let blk of block) {
				if (blk.x < 0 || blk.x >= horizontalTableBlocks ||
						blk.y >= verticalTableBlocks || table[blk.x][blk.y] !== -1) {
					_ok = false;
				}
			}
		}
		if (_ok === false) {
			if(isNewFigure) {
				gameEnds = true;
			} else {
				GetNewFigure(actualFigure, x, y);
				return;
			}
		} else {
			CheckBlocks();
		}
	}
	
	function HexToFigure(hex, x, y, isNewFigure){
		//IMPORTANT: block[0] must be at 2,2 (x,y)
		let actualColor = 0;
		if (isNewFigure) {
			actualColor = Math.floor((Math.random() * blockType.length));
		} else {
			actualColor = block[0].color;
		}
		let _dec = 0;
		let _ok = 3;
		for (let i = 0; i < 4; i++) {
			_dec = parseInt(hex.charAt(i), 16);
			if (_dec >= 8) {
				block[_ok].x = x + 2;
				block[_ok].y = y + i;
				block[_ok].color = actualColor;
				_dec -= 8;
				_ok--;
			}
			if (_dec >= 4) {
				block[_ok].x = x + 1;
				block[_ok].y = y + i;
				block[_ok].color = actualColor;
				_dec -= 4;
				_ok--;
			}
			if (_dec >= 2) {
				if (i !== 0) {
					block[_ok].x = x + 0;
					block[_ok].y = y + i;
					block[_ok].color = actualColor;
					_ok--;	
				}
				_dec -= 2;
			}
			if (_dec >= 1) {
				block[_ok].x = x - 1;
				block[_ok].y = y + i;
				block[_ok].color = actualColor;
				_dec--;
				_ok--;
			}
			if (_ok === 0) {
				if (actualFigure === 4) {
					block[_ok].x = x;
					block[_ok].y = y + 1;
				} else {
					block[_ok].x = x;
					block[_ok].y = y;
				}
				block[_ok].color = actualColor;
				break;
			}
		}
	}
	
	function Music(){
		if(mainBTN.innerHTML == mainButtonStopText)
		if(musicPlayer.paused){
			musicPlayer.play();
			musicBTN.innerHTML = musicBTNText + onString;
			localStorage.setItem("music","on");
		}else{
			musicPlayer.pause();
			musicBTN.innerHTML = musicBTNText + offString;
			localStorage.setItem("music","off");
		}
	}
	
	function SoundEffects(){
		if(mainBTN.innerHTML == mainButtonStopText)
			if(localStorage.getItem('sounds') == 'on'){
				soundBTN.innerHTML = soundBTNText + offString;
				localStorage.setItem('sounds','off');
			}else{
				soundBTN.innerHTML = soundBTNText + onString;
				localStorage.setItem('sounds','on');
			}
	}
	
	
	// ##################
	// ### PROTOTYPES ###
	
	CanvasRenderingContext2D.prototype.clear = CanvasRenderingContext2D.prototype.clear || function (preserveTransform) {
	    if (preserveTransform) {
	      this.save();
	      this.setTransform(1, 0, 0, 1, 0, 0);
	    }
	    this.clearRect(0, 0, this.canvas.width, this.canvas.height);
	    if (preserveTransform){
	      this.restore();
		}
	};
})();