# JSTetris2
**Current version: 2.0.9 beta**

JST2 is a program based on the popular game called Tetris that was made originally by Alexey Pajitnov and his co-workers. All of my code was writen in JavaScript and HTML5.

### Features
* Fully scripted game based on the Tetris
* Upper arrow can rotate figures (if there is space for this move)
* Game speeds up after some time
* Sound effects and looped music
* Special score effect when Tetris is achived
* Game can be paused by pressing `p` key

### How to use this application
You can easily include JST on your site:
1. Copy `JSTfiles` folder to your server.
2. Include `JSTetris2.js` to your project's code
3. Inside your code: make `div` container with id of `JST-Main`

or run example code from repository (open `index.html` in your browser).

<img src="screenshot.png" alt="JST game screen"/>